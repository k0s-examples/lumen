<?php
require_once('./fn/randomName.php');

if ($_SERVER['REQUEST_URI'] == '/version') {
    echo getenv('COMMIT');
} else {
    echo getRandomName();
}
